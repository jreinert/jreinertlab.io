title: Hi, I'm Jokke
---
# Hi, I'm Jokke

Checkout my [Gitlab profile](https://gitlab.com/jreinert)

## Find me on

- <a rel="me" href="https://bantha.tatooine.space/@jokke">Mastodon</a>
- <a rel="me" href="https://jreinert.keybase.pub">Keybase</a>
- <a rel="me" href="https://jreinert.github.io">Github</a>
